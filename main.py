
import sqlite3

# hardcoded path to the database file
database_file = "db/database.db"


# ------------------------------ functions -----------------------------------------

# this is a helper function that converts SQLite rows into dictionaries
def dict_from_row(row):
    return dict(zip(row.keys(), row)) 



# this function connects to the database
# it returns the connection object if successful
def connect_db(database_file):

    db_connection = sqlite3.connect(database_file)

    # this allows us to SELECT rows as dictionaries
    db_connection.row_factory = sqlite3.Row

    return db_connection



# this function stores the user data
def store_user_data(user_data):

    # ----------------  database setup -------------------
    # call the function to connect to the database
    db_connection = connect_db(database_file)

    # create a cursor object to hold our place as we work within the database
    db_cursor = db_connection.cursor()

    # try to populate the user row in database with the user_data dictionary
    try:
        # make the SQL statement
        sql = "INSERT INTO user (user_name,user_position) VALUES ('"+user_data["user_name"]+"','"+str(user_data["user_position"])+"')"
        # execute the SQL statement
        db_cursor.execute(sql)
        # write it to the database
        db_connection.commit()
        # add 'id' to the user_data dictionary so that the user's position can be updated later
        user_data["id"] = db_cursor.lastrowid
        # close database connection
        db_connection.close()

        print("Stored user data.")
        return user_data

    # if something fails then change the message so we don't assume it worked
    except sqlite3.Error as error:

        print("Database failed to update.",error)
        quit()
    

def save_user_position(user_data):

    # ----------------  database setup -------------------
    # call the function to connect to the database
    db_connection = connect_db(database_file)

    # create a cursor object to hold our place as we work within the database
    db_cursor = db_connection.cursor()

    # try to fetch the user info from the database
    try:
        # string for SQL command
        sql = "UPDATE user set user_position = ? where id = ?"

        # fills in ? for actual values
        data = (user_data["user_position"], user_data["id"])

        # execute and commit data to database
        db_cursor.execute(sql, data)
        db_connection.commit()

        print("\nGame saved for "+user_data["user_name"]+".")

        # close DB connection
        db_connection.close()


    except sqlite3.Error as error:

        print("Failed to save user position.", error)
        quit()
  



# this function gets all of the user data and returns it as a dictionary of dictionaries
# if it fails then it returns a False boolean
def get_all_user_data():

    # ----------------  database setup -------------------
    # call the function to connect to the database
    db_connection = connect_db(database_file)

    # create a cursor object to hold our place as we work within the database
    db_cursor = db_connection.cursor()

    # try to fetch the user info from the database
    try:
        
        # make the SQL statement to get the entire user table
        sql = "SELECT * FROM user"
        # execute the SQL statement
        db_cursor.execute(sql)

        # get the entire user table and assign it to 'allstuff'
        result = db_cursor.fetchall()

        # an empty dictionary to store dictionaries of user info
        all_user_data = {}

        # iterate through the SQL query results
        for row in result:
            
            # send each row off to our helper function to be converted into a dictionary
            this_user = dict_from_row(row)

            # grab the 'id' from this user
            this_id = this_user["id"]

            # add the dictionary of specific user data to our dictionary of all user data
            all_user_data[this_id] = this_user

        # close DB connection
        db_connection.close()

        # hand back all of the user data
        return all_user_data

    # if something fails then change the message so we don't assume it worked
    except sqlite3.Error as error:
        
        print("Failed to retreive user data from database.",error)
        quit()
    

# This just outputs the description of the current room. 
# It returns boolean True (succeed) or False (fail)
def read_description(user_data):

    # ----------------  database setup -------------------
    # call the function to connect to the database
    db_connection = connect_db(database_file)

    # create a cursor object to hold our place as we work within the database
    db_cursor = db_connection.cursor()

    # try to fetch the position description from the database
    try:
        
        # make the SQL statement 
        sql = "SELECT pos_description FROM position WHERE id = "+str(user_data["user_position"])
        # execute the SQL statement
        db_cursor.execute(sql)

        # fetch result and assign it to 'result'
        result = db_cursor.fetchone()
        # turn result into a dictionary
        result_dict = dict_from_row(result)

        print("\n"+result_dict["pos_description"])

        # close DB connection
        db_connection.close()

    except sqlite3.Error as error:

        print("Failed to get description from database.",error)
        quit()



def auto_position(user_data):

    # ----------------  database setup -------------------
    # call the function to connect to the database
    db_connection = connect_db(database_file)

    # create a cursor object to hold our place as we work within the database
    db_cursor = db_connection.cursor()

    # try to fetch the position info from the database
    try:
        
        # make the SQL statement to get the auto position integer if available
        sql = "SELECT pos_auto FROM position WHERE id = "+str(user_data["user_position"])
        # execute the SQL statement
        db_cursor.execute(sql)
        # get one row from the database
        result = db_cursor.fetchone()
        # turn result into a dictionary
        result_dict = dict_from_row(result)

        # print("pos_auto:",result_dict["pos_auto"])

        # close DB connection
        db_connection.close()

        return result_dict["pos_auto"]

    except sqlite3.Error as error:

        print("Failed to get auto-position from database.",error)
        quit()



# this function presents the options for the current position
def read_options(user_data):

    # ----------------  database setup -------------------
    # call the function to connect to the database
    db_connection = connect_db(database_file)

    # create a cursor object to hold our place as we work within the database
    db_cursor = db_connection.cursor()

    # try to fetch the option info from the database
    try:
            
        # print("read:",user_data)

        # make the SQL statement to get options available for each position
        sql = """SELECT 
        option.id, option_text, option_next_position 
        FROM option 
        INNER JOIN position_option 
        ON position_option.opt_id = option.id
        WHERE position_option.pos_id  = """ + str(user_data["user_position"])
        
        # print(sql)
        # execute the SQL statement
        db_cursor.execute(sql)

        # get the stuff
        result = db_cursor.fetchall()

        # empty option dictionary
        option_dict = {}

        # this is so that we can present the options in order from 1 to ?
        key = 1

        # iterate through the options pulled from DB
        for option in result:
            
            # convert row into a dictionary
            result_dict = dict_from_row(option)

            # add this row to the dictionary of options
            option_dict[key] = result_dict
            # list out the options
            options_text = "  "+str(key)+") "+result_dict["option_text"]
            print(options_text)

            # increment the key number
            key += 1

        # close DB connection
        db_connection.close()

        # print(option_dict)
        return option_dict

    except sqlite3.Error as error:

        print("Failed to get auto-position from database.",error)
        quit()


# -----------------------------------------------------------------------
# procedural code belongs beneath this line
if __name__ == "__main__":


    # ----------------  game setup -------------------
    # create an empty dictionary to store user data
    user_data = {}

    # flag to end loop once user has chosen to either renew a game or create a new one
    start_game_flag = False
    while start_game_flag == False:

        new_game = input("This is a simple RPG example. Would you like a new game? (y/n): ").lower()

        # if user wants to play a new game
        if new_game == "y":

            # get the new character name and place it in the starting position
            user_data["user_name"] = input("Great. Give us a cool character name: ").capitalize()
            user_data["user_position"] = 1

            # send user info off to be stored in the database.
            # It is returned with an 'id' for the new user 
            user_data = store_user_data(user_data)

            # flip the flag to end the loop
            start_game_flag = True

        # if user wants to retreive and old game
        elif new_game == "n":

            # get all of the user data from the database by calling appropriate functions
            # this will return a dictionary of dictionaries that contain user data from various characters
            # or False if the database call failed
            all_user_data = get_all_user_data()
            
            # if all_user_data isn't a False (fail) result
            if all_user_data: 

                # a string that we will concatenate programatically
                options = "\n"

                # iterate through all of the user data retreived from the database
                for id, user_info in all_user_data.items():
                    
                    # as we iterate through the database results, create a string of user options for them to choose from
                    options = options + str(user_info['id'])+") "+user_info['user_name']+", "

                # list out all of the characters they can choose from
                # the [:-2] bit is a string slice that removes the trailing comma and space from the loop above
                print(options[:-2])
                
                # flag to end loop once user has selected a character from database
                char_select_flag = False

                # loop 
                while char_select_flag == False:

                    try:
                        # let the user choose a character. if it can't be converted to an integer then this will fail.
                        this_id = int(input("Choose a previously saved character by number: "))

                        # create a dictionary with only the current user's data
                        user_data = all_user_data[this_id]
                        
                        # visibility of system status
                        out = "\nHey, "+user_data["user_name"]+", let's pick up where we left off."
                        print(out)

                        # ends all loops and allows the game to start
                        char_select_flag = True
                        start_game_flag = True
                        
                    except:
                        print("Choose a number in range please. Try again.")
        
        # if the user types an invalid input
        else:
            print("Just a 'y' or 'n' please. Try again.")


    # ----------------  game logic -------------------

    # flag to end game when user completes the game
    game_flag = True

    # instructions
    print("\nType 'q' at any time to save and quit the game.")

    # loop until game ends
    # this is a shortcut to test if a boolean is T or F. 
    # It could be written as:  while game_flag == True
    while game_flag:

        # print("user_data:",user_data)

        # send user data off to read the description of the space.
        read_description(user_data)

        # determine if auto
        pos_auto = auto_position(user_data)

        # if the position is one with the 'auto position' boolean True (1)
        if pos_auto == 1:
            user_data["user_position"] = pos_auto
            choice_flag = False
        # if it's not a position with an 'auto' 
        # then we have options that the user can choose from
        elif pos_auto == 0:
            choice_flag = True
        # when pos_auto comes back as -1 then we end the game
        else:
            choice_flag = False
            game_flag = False
            print("\nThe end!")

        # if the position has options
        if choice_flag:
            
            # give the user the options at that position
            option_dict = read_options(user_data)

            response_flag = False
            # the the user's response to the options
            while response_flag == False:

                try:
                    user_choice = input("Choose an option: ")

                    # if user types 'q' to quit
                    if user_choice.lower() == "q":
                        
                        # save user position in database
                        save_user_position(user_data)
                        # end this input loop
                        response_flag = True
                        # end game
                        game_flag = False

                    # otherwise, convert their choice to an int
                    else:
                        user_choice = int(user_choice)

                        # if the user actually chooses one of the available options
                        if user_choice in option_dict:
                            
                            # assign the next position to the user so that
                            # the next time it loops through the main game loop, it will 
                            # present the description and options of the next position
                            user_data["user_position"] = option_dict[user_choice]["option_next_position"]

                            # save user position in database
                            # save_user_position(user_data)

                            # end this input loop
                            response_flag = True

                        # if they choose a number outside of the range of options
                        else:
                            print("Type a number for one of the options above.")

                # if the user types in garbage that breaks the code
                except:
                    print("Just type a number to select a choice, please. Try again.")
