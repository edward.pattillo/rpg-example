# Basic RPG Example

This is just a mini-example of how to structure a text-based RPG game with persistent data.

It is as simple as possible but should serve as a proof-of-concept that anyone can expand upon. It uses only standard libraries so no need for a virtual environment in its current state.

To test this out, download it at the command prompt:

```
git clone git@gitlab.com:edward.pattillo/rpg-example.git
```

# Data Structure

To create the database we used the amazing (but quirky) [WWW SQL Designer](https://ondras.zarovi.cz/sql/demo/) tool by [Ondřej Žára](https://ondras.zarovi.cz/). I made a video on how to use this tool [here](https://drive.google.com/file/d/1vtWxFLOvEO71Jy5kuKoYe32oEJ03VIXs/view?usp=sharing).
![Alt text](https://i.imgur.com/bXXpcU7.png 'Data structure')

Notice the 'many to many' relationship between each position and the options available at each position. More info is in the datastore [here](https://gitlab.com/edward.pattillo/rpg-example/-/blob/master/db/datastore.py).

# Positions

![Alt text](https://i.imgur.com/15N5vU3.png 'Positions')

Open the position map [here](https://drive.google.com/file/d/1DMPKCqoOg3aQS9Pf0e8qtVf06AX1LQs7/view?usp=sharing).

# Change log

<ul>
<li>2021-05-24: Data structure, DB creation and DB population script</li>
<li>2021-05-25: Store user data for new game.</li>
<li>2021-05-26: Ask user for new game or retreive old game. If old game, retreive user data.</li>
<li>2021-05-27: Completely refactored data structure. Code is in a broken state at the moment.</li>
<li>2021-06-06: Code rewritten to accomodate relational data structure between positions and options.</li>
<li>2021-06-08: Fixed bug where new characters couldn't save ('q') game position.</li>
</ul>
