
# this python dictionary stores the game data that is populated into
# the 'position' table in the database
position = {

    # the numeric keys for each dictionary here are going to be mapped to the id column in the database
    1 : {
      "pos_description" : "You wake up to the sound of an alarm.",
      "pos_auto" : 0, # one is a boolean value for False which indicates that this position doesn't auto-forward to another
      "pos_options": 1 # one is a boolean value for True which indicates there are options for this position
        },

    2 : {
      "pos_description" : "You drop your closed fist towards the annoying sound. Somehow it lands in the right place. You drift back off to sleep.",
      "pos_auto" : 1, # here the 1 isn't a boolean, but points to the index of what position this leads to automatically.
      "pos_options": 0          
        },

    3 : {
      "pos_description" : "Enshrouded by brain fog, you lean forward. First your head comes up, then you let your wrists drop to the bed to push yourself the rest of the way up. You don't feel much energy in your arms.",
      "pos_auto" : 0,
      "pos_options": 1 # one is a boolean value for True which indicates there are options for this position
        },

    4 : {
      "pos_description" : "This just isn't going to happen. You crash down to your pillow, eyes closed and instantly fall back asleep.",
      "pos_auto" : 1,   
      "pos_options": 0          
        },

    5 : {
      "pos_description" : "With what seems like a huge amount of energy, you push yourself up. The room seems to spin around you while you shift your legs over the side of the bed. For some reason, this seems harder than ever before. You fall to the ground and lose consciousness. That's it.",
      "pos_auto" : -1,   # -1 signifies the end of the game
      "pos_options": 0
        }
  }

# this populates the option table in the database
# each option leads to the next position
option = {

    1 : { "option_text" : "Smash that alarm button.", "option_next_position" : 2 },
    2 : { "option_text" : "Try to get up.", "option_next_position" : 3 },
    3 : { "option_text" : "Give up.", "option_next_position" : 4 },
    4 : { "option_text" : "Keep trying.", "option_next_position" : 5 }
  }

# this is an intermediate table to store the "many to many" relationship linking position IDs to option IDs.
# Many different positions can have many different options.
position_option = {

  1 : { "pos_id" : 1, "opt_id" : 1},
  2 : { "pos_id" : 1, "opt_id" : 2},
  3 : { "pos_id" : 3, "opt_id" : 3},
  4 : { "pos_id" : 3, "opt_id" : 4}
}