"""
This script creates the sqlite3 database according to the schema.sql file in the same directory.

Then it populates the database with the dictionary in data.py

To run the script, open a Command Prompt in the 'db' folder, then type 'py populate_db.py'

"""

import sqlite3
import os

# hardcoded name of the database file
database_file = "database.db"
schema_file = "data_schema.sql"

# -----------------------------------------------------------------------
# this function connects to the database
# if the database.db file doesn't exist then it creates the database 
# it returns the connection object if successful
def connect_db(database_file):

    db_connection = sqlite3.connect(database_file)

    return db_connection


# -----------------------------------------------------------------------
# this function creates the data schema (tables and columns) in the database. 
# It accepts the DB connection as a parameter
def structure_db(db_connection,schema_file):

    # going to default to success. if there's an issue then we'll change the message
    message = "Database successfully structured."
    successFlag = True

    try:
        # your database schema must live in a file in the same directory
        with open(schema_file) as schema:
            db_connection.executescript(schema.read())

    except:
        message = "Database failed to be structured."
        successFlag = False
    
    return message, successFlag


# -----------------------------------------------------------------------
# this function iterates through 
def populate_db(cursor, table_name, table):

    # going to default to success. if there's an issue then we'll change the message
    message = "Database successfully populated."
    successFlag = True

    # iterate through the entire datastore by key,value
    for key, value in table.items():

        # print(key, value)

        # try to populate each row with a dictionary 'position'
        try:
            if table_name == "position":
                cursor.execute("INSERT INTO position VALUES (?,?,?,?) ", [key, value["pos_description"], value["pos_auto"], value["pos_options"]])

            elif table_name == "option":
                cursor.execute("INSERT INTO option VALUES (?,?,?) ", [key, value["option_text"], value["option_next_position"]])

            elif table_name == "position_option":
                cursor.execute("INSERT INTO position_option VALUES (?,?,?) ", [key, value["pos_id"], value["opt_id"]])

        # if something fails then change the message so we don't assume it worked
        except:
            
            message = "Database failed to populate."
            successFlag = False
    
    return message, successFlag



# -----------------------------------------------------------------------
# procedural code belongs beneath this line
if __name__ == "__main__":

    # check if database already exists
    if os.path.isfile(database_file):

        # file = open(database_file)

        # if the database file is there then prompt to overwrite it
        confirm = input("The database '"+database_file+"' already exists. Do you want to overwrite? (y/n): ")

        if confirm == "y":

            try:
                # delete existing database file
                os.remove(database_file)
            except:
                print("Failed to delete the old database file. \nCheck that it isn't connected in another app (eg. SQLiteStudio) then run this script again.")
                quit()

            # call the function to either connect to or create the database
            db_connection = connect_db(database_file)

        else:
            print("Ok, then. Goodbye.")
            quit() 

    else:

        # if the database file doesn't exist then 
        # call the function to either connect to or create the database
        db_connection = connect_db(database_file)




    # create a cursor object to hold our place as we work within the database
    cursor = db_connection.cursor()
    
    # call the function to structure the database with schema (tables and columns)
    # we return two variables (message and successFlag)
    message, successFlag = structure_db(db_connection,schema_file)

    print(message)




    # only proceed if structuring was successful
    if successFlag == True:
        # send the cursor we just created and the datastore dictionary off to populate the database
        from datastore import position,option,position_option # from a seperate file called datastore.py
        message, successFlag = populate_db(cursor,"position",position)
        message, successFlag = populate_db(cursor,"option",option)
        message, successFlag = populate_db(cursor,"position_option",position_option)

        # visibility of system status
        print(message)

        # only commit to database if populating was successful
        if successFlag == True:

            db_connection.commit()
            print("Done!")


